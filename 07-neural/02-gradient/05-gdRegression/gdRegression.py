import numpy as np
import gd

x = np.array([0, 1, 2, 3, 4], dtype=np.float32)
y = np.array([2, 3, 4, 5, 6], dtype=np.float32)

def predict(a, xt):
	return a[0]+a[1]*xt

def MSE(a, x, y):
	total = 0
	for i in range(len(x)):
		total += (y[i]-predict(a,x[i]))**2
	return total

def loss(p):
	return MSE(p, x, y)

p = [0.0, 0.0]
plearn = gd.gradientDescendent(loss, p, max_loops=3000, dump_period=1)
