# language model

輸入： train.txt
輸出： sample.txt

```
(env) mac020:01-language_model mac020$ python main.py
Epoch [1/5], Step[0/1], Loss: 2.1981, Perplexity:  9.01
Epoch [2/5], Step[0/1], Loss: 2.1399, Perplexity:  8.50
Epoch [3/5], Step[0/1], Loss: 2.0914, Perplexity:  8.10
Epoch [4/5], Step[0/1], Loss: 2.0619, Perplexity:  7.86
Epoch [5/5], Step[0/1], Loss: 2.0211, Perplexity:  7.55
Sampled [100/1000] words and save to sample.txt
Sampled [200/1000] words and save to sample.txt
Sampled [300/1000] words and save to sample.txt
Sampled [400/1000] words and save to sample.txt
Sampled [500/1000] words and save to sample.txt
Sampled [600/1000] words and save to sample.txt
Sampled [700/1000] words and save to sample.txt
Sampled [800/1000] words and save to sample.txt
Sampled [900/1000] words and save to sample.txt
Sampled [1000/1000] words and save to sample.txt
```


## 參考文獻

* https://github.com/pyliaorachel/resurrecting-the-dead-chinese.git

* [NLP 笔记 - Language models and smoothing](http://www.shuang0420.com/2017/02/24/NLP%20%E7%AC%94%E8%AE%B0%20-%20Language%20models%20and%20smoothing/)

Unknown word

对 unknown word 的处理，一般我们建一个固定大小的 lexicon(比如说语料库里 frequency>5 的单词)，再新建一个 token <UNK>，不在 lexicon 里的 token (也就是 frequency<5 的单词)都编译成 <UNK>，然后把 <UNK> 当做普通单词处理。